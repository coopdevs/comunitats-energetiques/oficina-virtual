import React, { useState, useEffect, Suspense } from "react";
import "./App.css";

import i18next from "i18next";

import CssBaseline from "@material-ui/core/CssBaseline";
import { Switch, Route, Redirect, useLocation } from "react-router-dom";

import { ThemeProvider } from "@material-ui/core/styles";
import { initialize } from "./initializers/";
import { FullScreenCenteredLayout } from "./components/layouts/FullScreenCenteredLayout";
import { ApplicationContext } from "./lib/ApplicationContext";
import { DependencyContext } from "./lib/DependencyContext";
import { whoami } from "./lib/api/auth";
import { useApplicationContext } from "./hooks/useApplicationContext";
import { mainTheme } from "./mainTheme";

import { MaintenanceMode } from "./screens/MaintenanceMode";
import { Home } from "./screens/Home/";
import { Login } from "./screens/Login";
import { Invoices } from "./screens/Invoices";
import { Products } from "./screens/Products";
import { PasswordReset } from "./screens/PasswordReset";
import { Profile } from "./screens/Profile";
import { ConfirmEmail } from "./screens/ConfirmEmail";
import { Spinner } from "./components/Spinner";
import { QueryClient, QueryClientProvider } from "react-query";
import { useHistory } from "react-router-dom";
import "console-goodies";

const queryClient = new QueryClient();

/**
 * Flip this boolean to enable/disable maintenance mode and deploy.
 */
const isMaintenanceMode = false;

const SplashScreen = () => (
  <FullScreenCenteredLayout>
    <Spinner />
  </FullScreenCenteredLayout>
);

const PrivateRoute = ({ children, ...rest }) => {
  const { currentUser } = useApplicationContext();
  const isAuthenticated = Boolean(currentUser);

  return (
    <Route
      {...rest}
      render={({ location }) =>
        isAuthenticated ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

const ScrollToTop = () => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
};

const AppRoutes = ({ onLogin }) => {
  if (isMaintenanceMode) {
    return <MaintenanceMode />;
  }

  return (
    <>
      <ScrollToTop />
      <Switch>
        <Route path="/login">
          <Login onLogin={onLogin} />
        </Route>

        <Route path="/password-reset">
          <PasswordReset />
        </Route>

        <Route path="/confirm-email/:token">
          <ConfirmEmail />
        </Route>

        <PrivateRoute path="/home">
          <Home />
        </PrivateRoute>

        <PrivateRoute path="/profile">
          <Profile />
        </PrivateRoute>

        <PrivateRoute path="/invoices">
          <Invoices />
        </PrivateRoute>

        <PrivateRoute path="/products">
          <Products />
        </PrivateRoute>

        <Route path="*" render={() => <Redirect to="/home" />} />
      </Switch>
    </>
  );
};

export function SomofficeShell(props) {
  const [isInitializing, setIsInitializing] = useState(true);
  const [contextValue, setContextValue] = useState({ currentUser: null });
  const history = useHistory();

  console.log(props.config);

  const getCurrentUser = async () => {
    let currentUser;
    try {
      currentUser = await whoami();
    } catch {
      currentUser = null;
    }

    return currentUser;
  };

  const onLogin = async () => {
    const currentUser = await getCurrentUser();
    setContextValue({ currentUser });
  };

  useEffect(() => {
    (async () => {
      await initialize();
      const currentUser = await getCurrentUser();

      if (currentUser) {
        setContextValue({ currentUser });
      } else {
        setIsInitializing(false);
      }
    })();
  }, []);

  useEffect(() => {
    (async () => {
      const { currentUser } = contextValue;
      const localeFromUrl = new URLSearchParams(window.location.search).get(
        "locale"
      );

      if (!currentUser) {
        return;
      }

      if (!Boolean(localeFromUrl)) {
        await i18next.changeLanguage(currentUser.preferred_locale);
      }

      setIsInitializing(false);
    })();
  }, [contextValue]);

  useEffect(() => {
    function trackMatomo(url) {
      const _paq = window._paq || [];
      _paq.push(["setCustomUrl", url]);
      _paq.push(["trackPageView", url]);
    }

    function trackGoogleAnalytics(url) {
      const ga = window.ga || function () {};

      ga("set", "page", url);
      ga("send", "pageview");
    }

    history.listen((location) => {
      const trackUrl = location.pathname + (location.search || "");

      trackMatomo(trackUrl);
      trackGoogleAnalytics(trackUrl);
    });
  }, [history]);

  return (
    <>
      <Suspense fallback={() => <SplashScreen />}>
        <ApplicationContext.Provider value={{ contextValue, setContextValue }}>
          <DependencyContext.Provider value={{ components: props.components }}>
            <QueryClientProvider client={queryClient}>
              <ThemeProvider theme={mainTheme(props.config)}>
                <CssBaseline />
                {!isMaintenanceMode && isInitializing ? (
                  <SplashScreen />
                ) : (
                  <AppRoutes onLogin={onLogin} />
                )}
              </ThemeProvider>
            </QueryClientProvider>
          </DependencyContext.Provider>
        </ApplicationContext.Provider>
      </Suspense>
    </>
  );
}
