import React, { useEffect, useMemo, useState } from "react";
import { filter, find, sortBy } from "lodash";
import { Select } from "components/Select";
import { Tiles } from "components/layouts/Tiles";
import { PricePreview } from "./PricePreview";
import { SliderSelect } from "components/SliderSelect";
import { formatBandwith } from "lib/helpers";
import Box from "@material-ui/core/Box";
import { Text } from "components/Text";
import { useTranslation } from "react-i18next";
import { Subheading } from "components/Subheading";
import { Field } from "react-final-form";
import { noop } from "lib/fn/noop";

const PRODUCT_QUERIES = {
  fiber: {
    category: "fiber",
    has_landline_phone: true
  },
  fiber_no_landline: {
    category: "fiber",
    has_landline_phone: false
  },
  adsl: {
    category: "adsl",
    has_landline_phone: false
  }
};

/**
 * Filters products from catalog from a set of predicates
 */
const ProductQuerySelector = ({ onChange, value }) => {
  const { t } = useTranslation();

  return (
    <Select
      onChange={onChange}
      value={value}
      label={t("funnel.tariffs.internet.what_do_you_want")}
      options={Object.entries(PRODUCT_QUERIES).map(([key, value]) => ({
        value,
        label: t(`funnel.tariffs.internet.categories.${key}`)
      }))}
    />
  );
};

const BandwidthSelector = ({ tariffs, value, onChange }) => {
  const items = sortBy(
    tariffs.map(tariff => ({
      value: Number(tariff.bandwidth),
      label: formatBandwith(tariff.bandwidth),
      code: tariff.code
    })),
    "value"
  );

  const { t } = useTranslation();

  return (
    <>
      <Box pb={1}>
        <Text size="xs">{t("funnel.tariffs.internet.what_speed")}</Text>
      </Box>
      <SliderSelect items={items} value={value} onChange={onChange} />
    </>
  );
};

const categoryForPriceTag = productQuery => {
  if (productQuery.category === "adsl") {
    return "adsl";
  }

  return "fiber" + (productQuery.has_landline_phone ? "" : "_no_landline");
};

export const InternetTariffPicker = ({
  tariffs,
  initialTariffCode,
  onChange = noop,
  showSubmit = false
}) => {
  const initialTariff = tariffs.find(({ code }) => code === initialTariffCode);

  const [productQuery, setProductQuery] = useState(
    initialTariff
      ? find(PRODUCT_QUERIES, {
          category: initialTariff.category,
          has_landline_phone: initialTariff.has_landline_phone
        })
      : PRODUCT_QUERIES.fiber
  );

  const filteredTariffs = filter(tariffs, productQuery);

  const [tariffCode, setTariffCode] = useState(
    initialTariff?.code || filteredTariffs[0].code
  );
  const [bandwidth, setBandwidth] = useState(
    initialTariff?.bandwidth || filteredTariffs[0].bandwidth
  );

  const matchingTariff = useMemo(() => {
    let result;

    if (productQuery.category === "adsl") {
      result = tariffs.find(tariff => tariff.code === tariffCode);
    } else {
      result = tariffs.find(
        tariff =>
          tariff.has_landline_phone === productQuery.has_landline_phone &&
          tariff.bandwidth === bandwidth
      );
    }

    return result || initialTariff;
  }, [tariffs, tariffCode, bandwidth, productQuery, initialTariff]);

  const { t } = useTranslation();

  useEffect(() => {
    if (!matchingTariff) {
      return;
    }

    onChange(matchingTariff);
  }, [matchingTariff]);

  return (
    <>
      <Box mb={4}>
        <Subheading color="text.main">
          {t("funnel.tariffs.internet.subtitle")}
        </Subheading>
      </Box>
      <Tiles spacing={4} columns={2}>
        <ProductQuerySelector
          value={productQuery}
          onChange={event => {
            const nextQuery = event.target.value;
            const nextDefaultTariff = find(tariffs, nextQuery);

            setTariffCode(nextDefaultTariff.code);
            setBandwidth(nextDefaultTariff.bandwidth);
            setProductQuery(nextQuery);
          }}
        />
        <div />
      </Tiles>
      {productQuery.category !== "adsl" && filteredTariffs.length > 1 && (
        <Box mt={4}>
          <BandwidthSelector
            value={bandwidth}
            onChange={({ value, code }) => {
              setBandwidth(value);
              setTariffCode(code);
            }}
            tariffs={filteredTariffs}
          />
        </Box>
      )}
      {productQuery.category === "adsl" && filteredTariffs.length > 1 && (
        <Box mt={4}>
          <Tiles spacing={4} columns={2}>
            <Select
              label="Selecciona tarifa de ADSL"
              value={initialTariffCode}
              options={filteredTariffs.map(tariff => ({
                value: tariff.code,
                label: tariff.name
              }))}
              onChange={event => {
                setTariffCode(event.target.value);
              }}
            />
          </Tiles>
        </Box>
      )}
      <Box mt={4}>
        <PricePreview
          category={categoryForPriceTag(productQuery)}
          details={["bandwidth"]}
          tariff={matchingTariff}
          showSubmit={showSubmit}
        />
      </Box>
    </>
  );
};

InternetTariffPicker.FormField = ({ name, validate, ...props }) => (
  <Field name={name} validate={validate}>
    {({ input, meta }) => (
      <InternetTariffPicker
        {...props}
        initialTariffCode={input.value}
        onChange={({ code }) => input.onChange(code)}
      />
    )}
  </Field>
);
