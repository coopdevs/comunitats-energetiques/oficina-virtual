import React from "react";
import { SidebarLayout } from "components/layouts/SidebarLayout";
import { Box, Card, Typography } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import { useApplicationContext } from "hooks/useApplicationContext";

const Index = () => {
  const { t } = useTranslation();
  const { path } = useRouteMatch();
  const { currentUser } = useApplicationContext();

  return (
    <Box component={Card} p={4}>
        <p>holaaaaa!</p>
    </Box>
  );
};

export const Products = () => {

  return (
    <Switch>
      <SidebarLayout>
        <Index />
      </SidebarLayout>
    </Switch>
  );
};
