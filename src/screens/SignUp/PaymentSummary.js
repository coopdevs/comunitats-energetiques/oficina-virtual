import React, { useState } from "react";
import { styled, Collapse, Box, Hidden } from "@material-ui/core";
import { Text } from "components/Text";
import { Separator } from "components/Separator";
import { Tiles } from "components/layouts/Tiles";
import { Stack } from "components/layouts/Stack";
import { useDerivedState, useStore } from "hooks/useStore";
import { compact } from "lodash";
import { useTranslation } from "react-i18next";
import { getLinesFromState } from "./shared/getLinesFromState";
import { formatPrice } from "lib/helpers";
import { useApplicationContext } from "hooks/useApplicationContext";

const ChevronDown = () => (
  <svg
    width="20"
    height="10"
    viewBox="0 0 20 10"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M1 1L10 9L19 1" stroke="white" />
  </svg>
);

const ChevronUp = () => (
  <svg
    width="20"
    height="10"
    viewBox="0 0 20 10"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M19 9L10 0.999999L1 9" stroke="white" />
  </svg>
);

const CircleCheckIcon = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <circle cx="10" cy="10" r="9.5" stroke="#F6DE59" />
    <path d="M5.88281 9.57646L8.997 13.5294L14.7063 6.47058" stroke="#F6DE59" />
  </svg>
);

const Root = styled("div")(({ theme }) => ({
  [theme.breakpoints.down("md")]: {
    background: theme.palette.primary.main,
    position: "fixed",
    top: 70,
    left: 0,
    width: "100%",
    zIndex: 1
    //background: theme.palette.primary.main,
    //overflow: 'hidden',
  },
  [theme.breakpoints.up("md")]: {
    position: "fixed",
    right: 0,
    top: 200,
    width: "300px",
    background: theme.palette.primary.main,
    borderTopLeftRadius: "5px",
    borderBottomLeftRadius: "5px",
    overflow: "hidden"
  }
}));

const getInitialPayment = (state, { mustPayMemberFee }) => {
  const lines = getLinesFromState(state);

  return compact([
    mustPayMemberFee && { concept: "new_signup", amount: 100 },
    ...lines
      .filter(line => line.type === "mobile" && !line.has_sim_card)
      .map(() => ({
        concept: "new_sim_card",
        amount: 2.05
      }))
  ]);
};

const getMonthlyBill = (state, tariffs) => {
  const lines = getLinesFromState(state);

  return lines.map(line => {
    const tariff = tariffs.find(t => t.code === line.code);

    return {
      concept: tariff.name,
      amount: Number(tariff.price)
    };
  });
};

// TODO duplicated from tariffs
const Reason = ({ text }) => (
  <Box display="flex" alignItems="center">
    <CircleCheckIcon />
    <Box ml={2}>
      <Text size="sm" color="white">
        {text}
      </Text>
    </Box>
  </Box>
);

const FullSummary = ({
  initialPayments,
  initialTotalAmount,
  monthlyPayments,
  monthlyTotalAmount
}) => {
  const { t } = useTranslation();
  return (
    <Box px={6} py={5}>
      <Tiles columns={1}>
        <Text color="white" size="sm" uppercase>
          {t("funnel.signup.payment_summary.title")}
        </Text>

        {initialPayments.length > 0 && (
          <>
            <Box my={1}>
              <Text color="white" size="md" semibold uppercase>
                {t("funnel.signup.payment_summary.initial_bill")}
              </Text>
            </Box>
            <Tiles columns={1} spacing={1}>
              {initialPayments.map(item => {
                return (
                  <Box display="flex" justifyContent="space-between">
                    <Text size="sm" color="white">
                      {t(
                        `funnel.signup.payment_summary.concepts.${item.concept}`
                      )}
                    </Text>
                    <Text size="sm" color="white">
                      {formatPrice(item.amount)}
                    </Text>
                  </Box>
                );
              })}
            </Tiles>
            <Separator variant="white" />
            <Box display="flex" justifyContent="space-between">
              <Text bold size="sm" color="white">
                {t("common.total")}
              </Text>
              <Text bold size="sm" color="white">
                {formatPrice(initialTotalAmount)}
              </Text>
            </Box>
          </>
        )}

        {monthlyPayments.length > 0 && (
          <>
            <Box my={1}>
              <div>
                <Text color="white" size="md" semibold uppercase>
                  {t("funnel.signup.payment_summary.monthly_bill")}
                </Text>
              </div>
              <div>
                <Text color="white" size="md" opacity={0.5}>
                  ({t("common.vat_included")})
                </Text>
              </div>
            </Box>

            <Tiles columns={1} spacing={1}>
              {monthlyPayments.map(item => {
                return (
                  <Box>
                    <Box display="flex" justifyContent="space-between">
                      <Text size="sm" color="white">
                        {item.concept}
                      </Text>
                      <Text size="sm" color="white">
                        {formatPrice(item.amount)}
                      </Text>
                    </Box>
                    <Box></Box>
                  </Box>
                );
              })}
            </Tiles>
            <Separator variant="white" />
            <Box display="flex" justifyContent="space-between">
              <Text size="sm" bold color="white">
                {t("common.total")}
              </Text>
              <Text size="sm" bold color="white">
                {formatPrice(monthlyTotalAmount)}
              </Text>
            </Box>
          </>
        )}
      </Tiles>
    </Box>
  );
};

export const MobileSummary = ({
  children,
  initialTotalAmount,
  monthlyTotalAmount
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const { t } = useTranslation();

  return (
    <Box minHeight="70px" height="auto" position="relative">
      <Box
        p={3}
        height="70px"
        display="flex"
        flexDirection="row"
        alignItems="center"
        position="absolute"
        justifyContent="space-between"
        width="100%"
        onClick={() => !isOpen && setIsOpen(true)}
      >
        <Box
          style={{ visibility: isOpen ? "hidden" : "visible" }}
          display="flex"
          flex={1}
        >
          <Stack spacing={0}>
            <Text size="sm" color="white" uppercase>
              {t("funnel.signup.payment_summary.initial_bill")}
            </Text>
            <Text bold size="sm" color="white">
              {formatPrice(initialTotalAmount)}
            </Text>
          </Stack>
        </Box>
        <Box
          style={{ visibility: isOpen ? "hidden" : "visible" }}
          display="flex"
          flex={1}
        >
          <Stack spacing={0}>
            <Text size="sm" color="white" uppercase>
              {t("funnel.signup.payment_summary.monthly_bill")}
            </Text>
            <Text bold size="sm" color="white">
              {formatPrice(monthlyTotalAmount)}
            </Text>
          </Stack>
        </Box>
        {!isOpen && (
          <Box display="flex">
            <ChevronDown />
          </Box>
        )}
        {isOpen && (
          <Box display="flex" onClick={() => setIsOpen(false)}>
            <ChevronUp />
          </Box>
        )}
      </Box>
      <Box width="100%">
        <Collapse in={isOpen}>{children}</Collapse>
      </Box>
    </Box>
  );
};

export const PaymentSummary = ({ tariffs }) => {
  const state = useStore(state => state);
  const { t } = useTranslation();
  const { mustPayMemberFee } = useDerivedState();

  const initialPayments = getInitialPayment(state, { mustPayMemberFee });
  const monthlyPayments = getMonthlyBill(state, tariffs);

  const computeTotal = list =>
    list.reduce((sum, { amount }) => sum + amount, 0);

  const initialTotalAmount = computeTotal(initialPayments);
  const monthlyTotalAmount = computeTotal(monthlyPayments);

  return (
    <>
      <Hidden mdDown>
        <Root>
          <FullSummary
            initialTotalAmount={initialTotalAmount}
            initialPayments={initialPayments}
            monthlyTotalAmount={monthlyTotalAmount}
            monthlyPayments={monthlyPayments}
          />
          <Box bgcolor="primary.dark" px={6} py={5}>
            <Tiles columns={1} spacing={1}>
              <Reason text={t("funnel.signup.payment_summary.reason_1")} />
              <Reason text={t("funnel.signup.payment_summary.reason_2")} />
              <Reason text={t("funnel.signup.payment_summary.reason_3")} />
            </Tiles>
          </Box>
        </Root>
      </Hidden>
      <Hidden mdUp>
        <Root>
          <MobileSummary
            initialTotalAmount={initialTotalAmount}
            monthlyTotalAmount={monthlyTotalAmount}
          >
            <FullSummary
              initialTotalAmount={initialTotalAmount}
              initialPayments={initialPayments}
              monthlyTotalAmount={monthlyTotalAmount}
              monthlyPayments={monthlyPayments}
            />
          </MobileSummary>
        </Root>
      </Hidden>
    </>
  );
};
