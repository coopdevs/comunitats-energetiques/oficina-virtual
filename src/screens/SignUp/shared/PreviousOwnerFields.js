import { Field, useForm, useFormState } from "react-final-form";
import { TextField } from "components/TextField";
import { Checkbox } from "components/Checkbox";
import { Condition } from "./Condition";
import {
  required,
  mustNotBe,
  mustMatchOther,
  composeValidators,
  matchVatFormat
} from "lib/form/validators";
import { TileSpan } from "components/layouts/Tiles";
import { useTranslation } from "react-i18next";
import { useApplicationContext } from "hooks/useApplicationContext";
import { useStore } from "hooks/useStore";

export const PreviousOwnerFields = () => {
  const { t } = useTranslation();

  const formStepDataByKey = useStore(state => state.formStepDataByKey);
  const { currentUser } = useApplicationContext();

  const currentUserVat =
    formStepDataByKey["partner/personal-data"]?.vat || currentUser.username;

  const disallowedVatValues = [
    currentUserVat,
    currentUserVat.replace(/^es/, "")
  ];

  return (
    <>
      <TileSpan span="all">
        <Checkbox.FormField
          name="other_person"
          label={t(
            "funnel.signup.data.steps.mobile_line_additional_data.other_person"
          )}
        />
      </TileSpan>
      <Condition when="other_person" is={true}>
        <Field
          type="hidden"
          name="previous_testing"
          component="input"
          initialValue="45771114x"
        />
        <TextField.FormField
          validate={composeValidators(
            mustNotBe({
              values: disallowedVatValues,
              errorKey: "vat_number_in_use"
            }),
            matchVatFormat
          )}
          name="previous_owner_vat"
          label={t(
            "funnel.signup.data.steps.mobile_line_additional_data.previous_owner_vat"
          )}
        />
        <TextField.FormField
          validate={required}
          name="previous_owner_name"
          label={t(
            "funnel.signup.data.steps.mobile_line_additional_data.previous_owner_name"
          )}
        />
        <TextField.FormField
          validate={required}
          name="previous_owner_surname"
          label={t(
            "funnel.signup.data.steps.mobile_line_additional_data.previous_owner_surname"
          )}
        />
        <TextField.FormField
          validate={required}
          name="previous_owner_lastname"
          label={t(
            "funnel.signup.data.steps.mobile_line_additional_data.previous_owner_lastname"
          )}
        />
      </Condition>
    </>
  );
};
