import React from "react";
import { Box } from "@material-ui/core";
import { Button } from "components/Button";
import { Tiles, TileSpan } from "components/layouts/Tiles";
import { TextField } from "components/TextField";
import { Checkbox } from "components/Checkbox";
import { Select } from "components/Select";
import { ApiSelect } from "../shared/ApiSelect";
import { AddressPicker } from "../shared/AddressPicker";
import { Condition } from "../shared/Condition";
import { useTranslation } from "react-i18next";
import {
  composeValidators,
  required,
  matchDateFormat,
  matchEmailFormat,
  matchVatFormat,
  mustMatchOther
} from "lib/form/validators";
import { previousBroadbandServices } from "lib/domain/somconnexio/selections";
import { getAvailableProviders } from "lib/api/availableProviders";
import { RadioToggle } from "components/RadioToggle";
import {PreviousOwnerFields} from "./PreviousOwnerFields";

export const InternetAdditionalData = () => {
  const { t } = useTranslation();

  return (
    <div>
      <Tiles columns={1} spacing={4}>
        <TileSpan span="all">
          <RadioToggle.FormField
            name="already_has_service"
            leftLabel={t(
              "funnel.signup.data.steps.internet_line_additional_data.already_has_service_yes"
            )}
            leftValue={true}
            rightLabel={t(
              "funnel.signup.data.steps.internet_line_additional_data.already_has_service_no"
            )}
            rightValue={false}
          />
        </TileSpan>
        <Condition when="already_has_service" is={true}>
          <Tiles columns={2} spacing={4}>
            <PreviousOwnerFields />
          </Tiles>
          <ApiSelect
            name="previous_provider"
            validate={required}
            label={t(
              "funnel.signup.data.steps.internet_line_additional_data.previous_provider"
            )}
            mapItem={item => ({ label: item.name, value: item.id })}
            query={() => getAvailableProviders({ category: "broadband" })}
          />
          <Select
            name="previous_broadband_service"
            validate={required}
            label={t("funnel.signup.data.steps.internet_line_additional_data.previous_broadband_service")}
            i18nPrefix="funnel.selections"
            options={previousBroadbandServices}
          />
          <Tiles columns={2} spacing={4}>
            <Condition when="internet_without_phone" is={false}>
              <TextField.FormField
                name="phone_number"
                validate={required}
                label={t(
                  "funnel.signup.data.steps.internet_line_additional_data.phone_number"
                )}
              />
            </Condition>
            <Checkbox.FormField
              name="internet_without_phone"
              label={t(
                "funnel.signup.data.steps.internet_line_additional_data.internet_without_phone"
              )}
            />
          </Tiles>
        </Condition>
        <TileSpan span="all">
          <AddressPicker
            label={t(
              "funnel.signup.data.steps.internet_line_additional_data.service_address"
            )}
            name="service_address"
          />
        </TileSpan>
        <Button type="submit">{t("common.continue")}</Button>
      </Tiles>
    </div>
  );
};
