import { buildAnalyticsParamFromState } from "./buildAnalyticsParamFromState";

describe("buildAnalyticsParamFromState", () => {
  let step = "thanks";
  it("works", () => {
    expect(
      buildAnalyticsParamFromState(
        {
          lines: [{ type: "mobile" }],
          currentRole: "member",
          optingForRole: "member"
        },
        step
      )
    ).toEqual("thanksmobiljasoci");
  });

  it("works", () => {
    expect(
      buildAnalyticsParamFromState(
        {
          lines: [{ type: "mobile" }],
          currentRole: "sponsored",
          optingForRole: "sponsored"
        },
        step
      )
    ).toEqual("thanksmobiljaapadrinat");
  });

  it("works", () => {
    expect(
      buildAnalyticsParamFromState({
        lines: [{ type: "mobile" }],
        currentRole: null,
        optingForRole: "member"
      }, step)
    ).toEqual("thanksmobilnovaaltamember");
  });

  it("works", () => {
    expect(
      buildAnalyticsParamFromState({
        lines: [{ type: "mobile" }],
        currentRole: null,
        optingForRole: "sponsored"
      }, step)
    ).toEqual("thanksmobilnovaaltasponsored");
  });

  it("works", () => {
    expect(
      buildAnalyticsParamFromState({
        lines: [{ type: "internet" }],
        currentRole: "sponsored",
        optingForRole: "sponsored"
      }, step)
    ).toEqual("thanksinternetjaapadrinat");
  });

  it("works", () => {
    expect(
      buildAnalyticsParamFromState({
        lines: [{ type: "internet" }],
        currentRole: null,
        optingForRole: "member"
      }, step)
    ).toEqual("thanksinternetnovaaltamember");
  });

  it("works", () => {
    expect(
      buildAnalyticsParamFromState({
        lines: [{ type: "internet" }],
        currentRole: null,
        optingForRole: "sponsored"
      }, step)
    ).toEqual("thanksinternetnovaaltasponsored");
  });

  it("works", () => {
    expect(
      buildAnalyticsParamFromState({
        lines: [{ type: "internet" }, { type: "mobile" }],
        currentRole: "member",
        optingForRole: "member"
      }, step)
    ).toEqual("thanksintmobijasoci");
  });

  it("works", () => {
    expect(
      buildAnalyticsParamFromState({
        lines: [{ type: "internet" }, { type: "mobile" }],
        currentRole: "sponsored",
        optingForRole: "sponsored"
      }, step)
    ).toEqual("thanksintmobijaapadrinat");
  });

  it("works", () => {
    expect(
      buildAnalyticsParamFromState({
        lines: [{ type: "internet" }, { type: "mobile" }],
        currentRole: null,
        optingForRole: "member"
      }, step)
    ).toEqual("thanksintmobinovaaltamember");
  });

  it("works", () => {
    expect(
      buildAnalyticsParamFromState({
        lines: [{ type: "internet" }, { type: "mobile" }],
        currentRole: null,
        optingForRole: "sponsored"
      }, step)
    ).toEqual("thanksintmobinovaaltasponsored");
  });

  it("works", () => {
    expect(
      buildAnalyticsParamFromState({
        lines: [],
        currentRole: null,
        optingForRole: "member"
      }, step)
    ).toEqual("thanksnocontractnovaaltamember");
  });
});
