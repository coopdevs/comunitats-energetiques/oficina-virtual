import { Box, Radio, withStyles, useTheme } from "@material-ui/core";
import { Tiles } from "components/layouts/Tiles";
import { Text } from "components/Text";
import { Field } from "react-final-form";

const StyledRadio = withStyles(theme => ({}))(Radio);

const UncheckedIcon = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <circle cx="10" cy="10" r="10" fill="white" />
    <circle cx="10" cy="10" r="9.5" stroke="#3E3382" stroke-opacity="0.7" />
  </svg>
);

const CheckedIcon = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <circle cx="10" cy="10" r="10" fill="white" />
    <circle cx="10" cy="10" r="9.5" stroke="#3E3382" stroke-opacity="0.7" />
    <circle cx="10" cy="10" r="5" fill="#3E3382" />
  </svg>
);

const RadioWrapper = ({ isSelected, onClick, radioValue, label }) => {
  const theme = useTheme();

  return (
    <Box
      onClick={onClick}
      px={3}
      display="flex"
      flexDirection="row"
      alignItems="center"
      height={theme.spacing(8)}
      bgcolor={isSelected ? "secondary.main" : "background.light"}
    >
      <Radio
        color="secondary"
        icon={<UncheckedIcon />}
        checkedIcon={<CheckedIcon />}
        checked={isSelected}
        value={radioValue}
      />
      <Box style={{opacity: isSelected ? 1.0 : 0.7}}>
        <Text>
          {label}
        </Text>
      </Box>
    </Box>
  );
};

export const RadioToggle = ({ value = true, onChange, name, leftValue, rightValue, leftLabel, rightLabel }) => {
  return (
    <Box borderRadius={5} overflow="hidden">
      <Tiles columns={2} spacing={0}>
        <RadioWrapper
          onClick={() => onChange(leftValue)}
          name={name}
          isSelected={value === leftValue}
          label={leftLabel}
        />
        <RadioWrapper
          onClick={() => onChange(rightValue)}
          name={name}
          isSelected={value === rightValue}
          label={rightLabel}
        />
      </Tiles>
    </Box>
  );
};

RadioToggle.FormField = ({name, validate, ...props }) => (
  <Field name={name} validate={validate}>
    {({ input, meta }) => (
      <RadioToggle
        {...props}

        value={input.value}
        onChange={input.onChange}
      />
    )}
  </Field>
)
