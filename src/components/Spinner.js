import React from "react";
import Box from "@material-ui/core/Box";
import CircularProgress from "@material-ui/core/CircularProgress";

export const Spinner = () => {
  return (
    <Box display="flex" flex={1} flexDirection="column">
      <Box display="flex" flex={1} justifyContent="center" alignItems="center">
        <CircularProgress />
      </Box>
    </Box>
  );
};
