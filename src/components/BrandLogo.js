import React from "react";
// import logo from "../../demo/public/logo.png"

export const BrandLogo = ({ width, height, src=undefined/*logo*/, alt='logo'}) => {
  return(
    <img 
      width={width}
      height={height}
      src={src}
      alt={alt}
    />
  );
}
