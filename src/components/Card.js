import React from "react";
import { Box, withStyles } from "@material-ui/core";
import { pick } from "lodash";
import { markAsCustomizable } from "../lib/markAsCustomizable";


const StyledBox = withStyles(({ palette }) => ({
  root: {
    cursor: ({ onClick }) => (Boolean(onClick) ? "pointer" : "auto"),
    borderRadius: 10,
    boxShadow: ({ shadow }) =>
      shadow ? "0px 20px 35px rgba(0, 0, 0, 0.15)" : "none",
    background: ({ bgcolor, isHighlighted }) =>
      bgcolor ||
      (isHighlighted ? palette.secondary.main : palette.background.light),
  }
}))(Box);

const paddingByVariant = {
  noGutter: {
    padding: 0,
  },
  default: {
    py: 8,
    px: [3, 8]
  },
  cta: {
    pt: 7,
    px: 5,
    pb: 5
  },
  formStep: {
    py: 0,
    px: 8
  }
};

const horizontal = padding => pick(padding, ["px"]);

export const Card = markAsCustomizable(
  "Card",
  ({
    variant = "default",
    header = null,
    shadow = true,
    headerBgcolor = "background.dark",
    children,
    topRightAction = null,
    ...props
  }) => (
    <StyledBox shadow={shadow} {...props}>
      {header && (
        <Box
          borderRadius='10px 10px 0 0'
          bgcolor={headerBgcolor}
          height="34px"
          color="text.main"
          py={2}
          {...horizontal(paddingByVariant[variant])}
          display="flex"
          flexDirection="row"
          alignItems="center"
        >
          {header}
        </Box>
      )}
      <Box
        position="relative"
        width="100%"
        height="100%"
        {...paddingByVariant[variant]}
      >
        <Box position="absolute" top={0} right={0} pr={3} pt={3}>
          {topRightAction}
        </Box>
        {children}
      </Box>
    </StyledBox>
  )
);
