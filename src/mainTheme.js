import { theme } from "./theme.js";
import { createTheme } from "@material-ui/core/styles";


export const mainTheme = (config) => {
    return createTheme({
        ...theme,
        palette: {
        ...theme.palette,
        primary: {
            ...theme.palette.primary,
            ...config?.theme?.palette?.primary,
        },
        },
        typography: {
            ...theme.typography,
            allVariants: {
                ...theme.typography.allVariants,                
                ...config?.theme?.typography?.allVariants,
            }
        },
    })
};
