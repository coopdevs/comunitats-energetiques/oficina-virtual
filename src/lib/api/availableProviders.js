import { get } from "axios";

export const getAvailableProviders = async ({ category = 'mobile' } = {}) => {
  const { data } = await get(`/api/providers/`, { params: { category } });

  try {
    return data;
  } catch (e) {
    console.error(e);
    /* handle error */
  }
};
