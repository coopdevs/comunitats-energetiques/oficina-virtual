import axios, { get } from "axios";
import { LOGIN_ENDPOINT, CHECK_AUTH_ENDPOINT } from "lib/api/auth";

const setupCSRF = async () => {
  await get("/api/auth/set-csrf-cookie");

  axios.defaults.xsrfCookieName = "csrftoken";
  axios.defaults.xsrfHeaderName = "X-CSRFToken";
};

/**
 * This endpoints should be ignored by the sessions expired interceptor.
 */
const skipEndpoints = [LOGIN_ENDPOINT, CHECK_AUTH_ENDPOINT];

const setupSessionExpiredInterceptor = () => {
  axios.interceptors.response.use(
    res => res,
    err => {
      if (skipEndpoints.includes(err.config.url)) {
        return Promise.reject(err);
      }

      if (err.response.status === 403 && err?.response?.data?.reason === "authentication_required") {
        window.location.reload();
        return;
      }

      return Promise.reject(err)
    }
  );
};

export async function initializeAxios() {
  // setupSessionExpiredInterceptor();
  // await setupCSRF();

  window.axios = axios;
}
